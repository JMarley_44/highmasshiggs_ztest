package runsimulation;
class EnergyLoss
{
    double rho;
    double Z;
    double A;
    double K;
    double me;
    double I;
    
    public EnergyLoss(double rho_in, double Z_in, double A_in)
    {
        rho = rho_in;
        Z = Z_in;
        A = A_in;
        
    }
    
     public double getWmax (Particle part, double emass)
    {
     double varA = 2*me*part.beta()*part.beta()*part.gamma()*part.gamma();
     double varB = (2*part.gamma()*me)/(part.m);
     double varC = (me*me)/(part.m*part.m);
        double Wmax = varA/(1+varB+varC);
        return Wmax;
    }
     
    public double getEnergyLoss(Particle p)//, double K_in, double emass, double I_in, double Wmax)
    {
        if( A==0 || Z == 0){ 
            return 0;
        }
        K =0.307075;
        me = 0.511;
        I = 0.0000135*Z;
        double Wmax = getWmax(p,me);
         // shall return energy loss in MeV/m
         double var1 = 100*K*p.Q*p.Q*rho;
         double var2 = Z/(A*p.beta()*p.beta());
         double var3 = 2*me*p.beta()*p.beta()*p.gamma()*p.gamma()*Wmax/(I*I);
         double var4 = (0.5*Math.log(var3))-(p.beta()*p.beta());
                
        double EnergyLoss = var1 * var2 * var4;
        return EnergyLoss;
    }
    
}

